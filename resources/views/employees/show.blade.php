@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">

        @if(Auth::user()->roles()->where('name', 'superadmin')->exists() || Auth::user()->roles()->where('name',
        'admin')->exists()|| Auth::user()->roles()->where('name',
        'company')->exists())
        <span class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('employees.index') }}"> Back</a>
        </span>
        @endif

        @if(Auth::user()->roles()->where('name', 'employee')->exists())
        <span class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('employees.edit', $employee->id) }}"> Edit</a>
        </span>
        @endif

        <div class="pull-left">
            <h2> {{ $employee->firstName." ".$employee->lastName }} Detail</h2>
        </div>



    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $employee->firstName }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Details:</strong>
            {{ $employee->lastName }}
        </div>
    </div>
</div>
@endsection
