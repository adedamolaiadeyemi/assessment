@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Employee</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('employees.index') }}"> Back</a>
        </div>
    </div>
</div>


@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('employees.update',$employee->id) }}" method="POST">
    @csrf
    @method('PUT')


    <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>First Name:</strong>
                <input type="text" name="firstName" value="{{ $employee->firstName }}" class="form-control"
                    placeholder="First Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Last Name:</strong>
                <input type="text" name="lastName" value="{{ $employee->lastName }}" class="form-control"
                    placeholder="Last Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="email" name="email" value="{{ $employee->email }}" required class="form-control"
                    placeholder="Email">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Phone Number:</strong>
                <input type="text" name="phone" value="{{ $employee->phone }}" class="form-control"
                    placeholder="Phone Number">
            </div>
        </div>

        @if(Auth::user()->roles()->where('name', 'company')->exists()||Auth::user()->roles()->where('name',
        'employee')->exists() )
        <input type="hidden" name="company_id" required value="{{ $company->id }}">
        @endif

        @if(Auth::user()->roles()->where('name', 'admin')->exists() || Auth::user()->roles()->where('name',
        'superadmin')->exists() )
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Company:</strong>
                <select name="company_id" class="form-control" id="company_id" required>
                    <option value=""></option>

                    <option value=""></option>
                    @foreach($company as $display)
                    @if($display->id == $employee->company_id)
                    <option selected value="{{ $display->id }}">{{ $display->name }}</option>
                    @else
                    <option value="{{ $display->id }}">{{ $display->name }}</option>
                    @endif



                    @endforeach




                </select>
            </div>
        </div>
        @endif






        {{--
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" value="{{ $employee->name }}" class="form-control" placeholder="Name">
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Detail:</strong>
            <textarea class="form-control" style="height:150px" name="detail"
                placeholder="Detail">{{ $employee->detail }}</textarea>
        </div>
    </div> --}}
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </div>


</form>


@endsection
