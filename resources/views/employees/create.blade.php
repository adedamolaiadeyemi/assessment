@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New employee</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('employees.index') }}"> Back</a>
        </div>
    </div>
</div>


@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


<form action="{{ route('employees.store') }}" method="POST">
    @csrf


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>First Name:</strong>
                <input type="text" required name="firstName" class="form-control" placeholder="First Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Last Name:</strong>
                <input type="text" name="lastName" class="form-control" placeholder="Last Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="email" name="email" required class="form-control" placeholder="Email">
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Phone Number:</strong>
                <input type="text" name="phone" class="form-control" placeholder="Phone Number">
            </div>
        </div>

        @if(Auth::user()->roles()->where('name', 'company')->exists() )
        <input type="hidden" name="company_id" required value="{{ $company->id }}">
        @endif

        @if(Auth::user()->roles()->where('name', 'admin')->exists() || Auth::user()->roles()->where('name',
        'superadmin')->exists() )
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>Company:</strong>
                <select name="company_id" class="form-control" id="company_id" required>
                    <option value=""></option>
                    @foreach($company as $display)
                    <option value="{{ $display->id }}">{{ $display->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>


</form>

@endsection
