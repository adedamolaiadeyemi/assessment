@extends('layouts.app')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>employees</h2>
        </div>
        <div class="pull-right">
            @can('employee-create')
            <a class="btn btn-success" href="{{ route('employees.create') }}"> Create New employee</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Company</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($employees as $employee)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $employee->firstName  }} {{ $employee->lastName }}</td>
        <td>{{ $employee->email }}</td>
        <td>{{ $employee->phone }}</td>
        <td>{{ $employee->company->name }}</td>
        <td>
            <form action="{{ route('employees.destroy',$employee->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('employees.show',$employee->id) }}">Show</a>
                @can('employee-edit')
                <a class="btn btn-primary" href="{{ route('employees.edit',$employee->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('employee-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $employees->links() !!}



@endsection
