@extends('layouts.app')


@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">

        @if(Auth::user()->roles()->where('name', 'superadmin')->exists() || Auth::user()->roles()->where('name',
        'admin')->exists())
        <span class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('companies.index') }}"> Back</a>
        </span>
        @endif

        @if(Auth::user()->roles()->where('name', 'company')->exists())
        <span class="pull-right" style="float: right;">
            <a class="btn btn-primary" href="{{ route('companies.edit', $company->id) }}"> Edit</a>
        </span>
        @endif

        <div class="pull-left">
            <h2> {{ $company->name }} Detail</h2>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $company->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Email:</strong>
            {{ $company->email }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Web Site:</strong>
            {{ $company->website }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Logo:</strong><br />
            <img src="{{ URL::asset('storage/'.$company->logo) }}" width=100 class="img-thumbnail" />
        </div>
    </div>
</div>
@endsection
