<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuditController;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/linkmigrate', function () {
    Artisan::call('migrate');
});
Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});

Route::get('audits', 'App\Http\Controllers\AuditController@index')
    ->middleware('auth', \App\Http\Middleware\AllowOnlyAdmin::class)->name('audits');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('companies', 'App\Http\Controllers\CompanyController'::class);
    Route::resource('employees', 'App\Http\Controllers\EmployeeController'::class);
});