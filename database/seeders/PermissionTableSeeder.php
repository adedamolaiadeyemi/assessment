<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'employee-list',
            'employee-create',
            'employee-edit',
            'employee-delete',
            'company-list',
            'company-create',
            'company-edit',
            'company-delete'
        ];

        foreach ($permissions as $permission) {
            permission::create(['name' => $permission]);
        }
    }
}