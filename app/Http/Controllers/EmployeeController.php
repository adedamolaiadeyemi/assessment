<?php

namespace App\Http\Controllers;

use App\Models\company;
use App\Models\employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Mailgun\Mailgun;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:employee-list|employee-create|employee-edit|employee-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:employee-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:employee-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:employee-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->roles()->where('name', 'employee')->exists()) {
            $employee = DB::table('employees')->where('user_id', Auth::user()->id)->first();
            return view('employees.show', compact('employee'));
        }
        if (Auth::user()->roles()->where('name', 'company')->exists()) {
            $company = company::with(['employees'])->where('user_id', Auth::user()->id)->first();
            //$company->dd();
            ///$employees = $company->employees->all();
            $employees = employee::with(['company'])->where('company_id', '=', $company->id)->latest()->paginate(5);
        }
        if (
            Auth::user()->roles()->where('name', 'superadmin')->exists() ||
            Auth::user()->roles()->where('name', 'admin')->exists()
        ) {
            $employees = employee::latest()->paginate(5);
        }
        return view('employees.index', compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (Auth::user()->roles()->where('name', 'admin')->exists() || Auth::user()->roles()->where('name', 'superadmin')->exists()) {
            $company = company::get();
        }
        if (Auth::user()->roles()->where('name', 'company')->exists()) {
            $company = company::where('user_id', '=', Auth::user()->id)->first();
        }
        return view('employees.create', compact('company'));
        // return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'firstName' => 'required',
            'company_id' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $password = Str::random(8) . "@" . Str::random(2);
        $user = User::create([
            'name' => $input['firstName'] . " " . $input['lastName'],
            'email' => $input['email'],
            'password' => Hash::make($password) // bcrypt('password')
        ]);
        $role = Role::where('name', '=', 'employee')->pluck('id', 'id')->all();
        $user->assignRole($role);


        $user = employee::create([
            'firstName' => $input['firstName'],
            'lastName' => $input['lastName'],
            'email' => $input['email'],
            'company_id' => $input['company_id'],
            'phone' => $input['phone'],
            'user_id' => $user->id
        ]);

        $toEmail = $input['email'];
        $mailBody = 'Hi ' . $input['firstName'] . ',' . '<br/><br/>Please note that a Glade Test Account was created for you successfuly! please find your login detail below<br/><br/>User Name:<b>' . $input['email'] . '</b><br/>Passord:<b>' . $password . '</b><br/><br/><br/>Thank You.';

        Mail::html($mailBody, function ($message) use ($toEmail) {
            $message->subject('Glade Test Account Creation!');
            $message->from('no-reply@gladetest.com', config('app.name', 'Laravel'));
            $message->to(env('APP_EMAIL_TEST', $toEmail));
        });

        var_dump(Mail::failures());

        return redirect()->route('employees.index')
            ->with('success', 'Employee created successfully.');
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(employee $employee)
    {
        return view('employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(employee $employee)
    {
        if (Auth::user()->roles()->where('name', 'admin')->exists() || Auth::user()->roles()->where('name', 'superadmin')->exists()) {
            $company = company::get();
        }
        if (Auth::user()->roles()->where('name', 'company')->exists()) {
            $company = company::where('user_id', '=', Auth::user()->id)->first();
        }
        if (Auth::user()->roles()->where('name', 'employee')->exists()) {
            $company = company::where('id', '=', $employee->company_id)->first();
        }
        return view('employees.edit', compact('employee'))->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employee $employee)
    {
        request()->validate([
            'firstName' => 'required',
            'company_id' => 'required',
            'email' => 'required',
        ]);
        $input = $request->all();
        $employee->update([
            'firstName' => $input['firstName'],
            'lastName' => $input['lastName'],
            'email' => $input['email'],
            'company_id' => $input['company_id'],
            'phone' => $input['phone']
        ]);

        $User = user::where('id', $employee->user_id)->first();

        $User->update([
            'name' => $input['firstName'] . " " . $input['lastName'],
            'email' => $input['email']
        ]);


        return redirect()->route('employees.index')
            ->with('success', 'employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(employee $employee)
    {

        user::where('id', $employee->user_id)->delete();

        return redirect()->route('employees.index')
            ->with('success', 'employee deleted successfully');
    }
}