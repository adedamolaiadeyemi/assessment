<?php

namespace App\Http\Controllers;

use App\Models\company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\employee;
use Spatie\Permission\Models\Role;
use DB;
use Facade\FlareClient\Stacktrace\File;
use Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Mailgun\Mailgun;




class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:company-list|company-create|company-edit|company-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:company-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:company-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->roles()->where('name', 'employee')->exists()) {
            $employee = DB::table('employees')->where('user_id', Auth::user()->id)->first();
            $company = DB::table('companies')->where('id', $employee->company_id)->first();
            return view('companies.show', compact('company'));
        }
        if (Auth::user()->roles()->where('name', 'company')->exists()) {
            $company = DB::table('companies')->where('user_id', Auth::user()->id)->first(); //company::where('user_id', Auth::user()->id)->first();;
            return view('companies.show')->with('company', $company);
        }

        $companies = company::with('employees')->latest()->paginate(5);
        return view('companies.index', compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $password = Str::random(8) . "@" . Str::random(2);

        $input = $request->all();
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($password) // bcrypt('password')
        ]);
        $role = Role::where('name', '=', 'company')->pluck('id', 'id')->all();
        $user->assignRole($role);


        $storeImageName = "";
        if ($request->file('logoFile') != null) {
            $storeName = $request->file('logoFile')->store('public');
            $storeImageName = str_ireplace("public/", "", $storeName);
        }

        $user = company::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'website' => $input['website'],
            'logo' => $storeImageName,
            'user_id' => $user->id
        ]);

        $toEmail = $input['email'];
        $mailBody = 'Hi ' . $input['name'] . ',' . '<br/><br/>Please note that a Glade Test Account was created for you successfuly! please find your login detail below<br/><br/>User Name:<b>' . $input['email'] . '</b><br/>Passord:<b>' . $password . '</b><br/><br/><br/>Thank You.';

        Mail::html($mailBody, function ($message) use ($toEmail) {
            $message->subject('Glade Test Account Creation!');
            $message->from('no-reply@gladetest.com', config('app.name', 'Laravel'));
            $message->to(env('APP_EMAIL_TEST', $toEmail));
        });

        var_dump(Mail::failures());

        return redirect()->route('companies.index')
            ->with('success', 'company created successfully.');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(company $company)
    {
        return view('companies.show', compact('company'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(company $company)
    {
        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, company $company)
    {


        request()->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $input = $request->all();
        $storeImageName = $company->logo;

        if ($request->file('logoFile') != null) {

            $storeName = $request->file('logoFile')->store('public');
            $storeImageName = str_ireplace("public/", "", $storeName);

            $path = public_path("storage/$company->logo");
            if (is_file($path)) {
                unlink($path);
            }
        }


        $company->update([
            'name' => $input['name'],
            'email' => $input['email'],
            'website' => $input['website'],
            'logo' => $storeImageName
        ]);

        $user = user::where('id', $company->user_id)->first();

        $user->update([
            'name' => $input['name'],
            'email' => $input['email'],
        ]);



        return redirect()->route('companies.index')
            ->with('success', 'company updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(company $company)
    {
        user::where('id', $company->user_id)->delete();

        return redirect()->route('companies.index')
            ->with('success', 'company deleted successfully');
    }
}