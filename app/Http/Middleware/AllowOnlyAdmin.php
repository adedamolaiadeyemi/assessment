<?php
    
    namespace App\Http\Middleware;
    
    use Closure;
    use Illuminate\Support\Facades\Auth;
    
    class AllowOnlyAdmin
    {
        public function handle($request, Closure $next)
        {
            //if (Auth::user()->is_admin) {
            if (Auth::user()->roles()->where('name', 'superadmin')->exists()) {
                
                return $next($request);
            }
            
            abort(403);
        }
    }